﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMesh : MonoBehaviour
{
    [SerializeField]
    private new Collider collider;
    public Collider Collider
    {
        get
        {
            return collider;
        }
    }

    [SerializeField]
    private new Renderer renderer;
    public Renderer Renderer
    {
        get
        {
            return renderer;
        }
    }

    public bool Interactable
    {
        get
        {
            return Collider.enabled;
        }
        set
        {
            Collider.enabled = value;
        }
    } 
}
