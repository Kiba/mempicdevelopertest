﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Source
{
    public static Coroutine DeferredAction(this MonoBehaviour monoBehaviour, float time, Action action)
    {
        return monoBehaviour.StartCoroutine(DeferredAction(time, action));
    }

    public static IEnumerator DeferredAction(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        action();
    }

    public static Coroutine RecurringActions(this MonoBehaviour monoBehaviour, Func<bool> func)
    {
        return monoBehaviour.StartCoroutine(RecurringActions(func));
    }

    public static IEnumerator RecurringActions(Func<bool> func)
    {
        while (func())
        {
            yield return null;
        }       
    }

    public static IEnumerator Move(Transform transform, Vector3 target, float time)
    {
        float startTime = Time.time;
        float deltaTime;

        while ((deltaTime = (Time.time - startTime)) <= time)
        {
            transform.position = Vector3.Lerp(transform.position, target, deltaTime / time);
            yield return null;
        }
        transform.position = target;
    }   
}
