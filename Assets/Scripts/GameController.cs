﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// разбивать на компоненты не понадобилось, но обычно стараюсь сделать классы как можно меньше по размеру и ускоспециализированные
/// </summary>
public class GameController : MonoBehaviour
{
    /// <summary>
    /// true - stop the "tap and hold" logic if the touch leave the Mesh
    /// </summary>
    [SerializeField, Tooltip("если true - останавливает установку мэша")]
    private bool stopTrackingOnLeaveMesh = true;
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private GameMesh meshPrefab;
    /// <summary>
    /// стартовый мэш на игровом поле
    /// </summary>
    [SerializeField, Tooltip("стартовый мэш на игровом поле")]
    private GameMesh baseMesh;
    /// <summary>
    /// цвет который принимает мэш при ошибке
    /// </summary>
    [SerializeField, Tooltip("цвет который принимает мэш при ошибке")]
    private Material mashFail;
    /// <summary>
    /// тэг игрового обьекта считающегося мэшэм
    /// </summary>
    [SerializeField, Tooltip("тэг игрового обьекта считающегося мэшэм")]
    private string meshTag = "Mesh";
    /// <summary>
    /// отступ установки нового мэша
    /// </summary>
    [SerializeField, Tooltip("отступ установки нового мэша")]
    private Vector3 meshOffset = new Vector3(0, 0.25f, 0);
    /// <summary>
    /// максимально допустимое превышение мэша относительно предыдущего
    /// </summary>
    [SerializeField, Tooltip("максимально допустимое превышение мэша относительно предыдущего")]
    private float maxStepRatio = 1.1f;
    /// <summary>
    /// скорость изменения размера нового мэша, при удержании тапа
    /// </summary>
    [SerializeField, Tooltip("скорость изменения размера нового мэша, при удержании тапа")]
    private float installationSpeed = 1;
    /// <summary>
    /// область для Perfect Move
    /// </summary>
    [SerializeField, Tooltip("область для Perfect Move")]
    private float perfectMoveRange = 0.05f;
    /// <summary>
    /// время анимации Perfect Move, в большую сторону - X, в меньшую Y
    /// </summary>
    [SerializeField, Tooltip("время анимации Perfect Move, в большую сторону - X, в меньшую Y")]
    private Vector2 perfectMoveAnimationTime = new Vector2(0.5f, 0.3f);
    /// <summary>
    /// размер анимации Perfect Move, в большую сторону - X, в меньшую Y
    /// </summary>
    [SerializeField, Tooltip("размер анимации Perfect Move, в большую сторону - X, в меньшую Y")]
    private Vector2 perfectMoveAnimationRange = new Vector2(0.4f, -0.2f);
    /// <summary>
    /// время между анимациями в цепочке, при Perfect Move
    /// </summary>
    [SerializeField, Tooltip("время между анимациями в цепочке, при Perfect Move")]
    private float timeBetweenAnimations = 0.2f;
    /// <summary>
    /// время показа 'не правильного' блока, до его уничтожения
    /// </summary>
    [SerializeField, Tooltip("время показа 'не правильного' блока, до его уничтожения")]
    private float failedObjectLifetime = 0.7f;
    /// <summary>
    /// время движения камеры на новую позицию, после установки нового мэша
    /// </summary>
    [SerializeField, Tooltip("время движения камеры на новую позицию, после установки нового мэша")]
    private float cameraStepTime = 0.4f;
    /// <summary>
    /// максимально допустимый размер мэша
    /// </summary>
    [SerializeField, Tooltip("максимально допустимый размер мэша")]
    private float maxMeshSize = 1;
    /// <summary>
    /// минимально допустимый размер мэша
    /// </summary>
    [SerializeField, Tooltip("минимально допустимый размер мэша")]
    private float minMeshSize = 0.1f;

    /// <summary>
    /// позиция X и Z на которую переместится камера при показе башни 
    /// </summary>
    [SerializeField, Tooltip("позиция X и Z на которую переместится камера при показе башни ")]
    private Vector3 cameraShowTowerPosition = new Vector3(0, 0, -10);
    /// <summary>
    /// время за которое камера переместится на позицию для показа башни 
    /// </summary>
    [SerializeField, Tooltip("время за которое камера переместится на позицию для показа башни ")]
    private float cameraShowTowerWayTime = 1.2f;

    /// <summary>
    /// Объект вызивающий функцию RestartGame
    /// </summary>
    [SerializeField, Space(30), Tooltip("Объект вызивающий функцию RestartGame")]
    private GameObject restartCaller;

    private bool interactable = true;

    private GameMesh previousMesh;
    private List<GameMesh> meshes = new List<GameMesh>();

    private GameMesh previousPerfectMesh;

    private bool isHold = false;

    private Coroutine meshInstallation;

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void Start()
    {
        previousMesh = baseMesh;
        meshes.Add(previousMesh);
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (interactable && Input.GetMouseButton(0))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                OnMeshHold(hit.collider != null && hit.collider.tag == meshTag);
            }
            else if (stopTrackingOnLeaveMesh)
            {
                OnMeshHold(false);
            }
        }
        else
        {
            OnMeshHold(false);
        }
#else
        if (interactable && Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began || stopTrackingOnLeaveMesh && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Ray ray = mainCamera.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    OnMeshHold(hit.collider != null && hit.collider.tag == meshTag);
                }
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled)
            {
                OnMeshHold(false);
            }
        }
#endif
    }

    private void OnMeshHold(bool isHold)
    {
        this.isHold = isHold;

        if (isHold && meshInstallation == null)
        {
            meshInstallation = StartCoroutine(MeshInstallationCoroutine());
        }
    }

    private IEnumerator MeshInstallationCoroutine()
    {
        GameMesh currentMesh = Instantiate(meshPrefab);
        currentMesh.transform.position = previousMesh.transform.position + meshOffset;
        currentMesh.transform.localScale = new Vector3(0, currentMesh.transform.localScale.y, 0);
        currentMesh.Interactable = false;

        Vector3 installationSpeed = new Vector3(this.installationSpeed, 0, this.installationSpeed);
        while (true)
        {
            currentMesh.transform.localScale += installationSpeed * Time.deltaTime;
            yield return null;

            if (currentMesh.transform.localScale.x >= maxStepRatio * previousMesh.transform.localScale.x)
            {
                GameOver(currentMesh);
                break;
            }

            if (!isHold)
            {
                if (currentMesh.transform.localScale.x >= previousMesh.transform.localScale.x)
                {
                    GameOver(currentMesh);
                }
                else
                {
                    Action end = () =>
                    {
                        previousMesh.Interactable = false;
                        currentMesh.Interactable = true;

                        meshes.Add(currentMesh);
                        previousMesh = currentMesh;

                        StartCoroutine(Source.Move(mainCamera.transform, mainCamera.transform.position + meshOffset, cameraStepTime));

                        meshInstallation = null;
                    };

                    if ((previousMesh.transform.localScale.x - currentMesh.transform.localScale.x) <= perfectMoveRange)
                    {
                        end();
                        PerfectMove();
                        yield break;
                    }

                    end();
                }
                break;
            }
        }
    }

    private void GameOver(GameMesh failedObject)
    {
        interactable = false;
        failedObject.Renderer.material = mashFail;
        Destroy(failedObject, failedObjectLifetime);

        Vector3 target = meshes[meshes.Count / 2].transform.position;
        cameraShowTowerPosition.y = target.y;

        StartCoroutine(Source.Move(mainCamera.transform, cameraShowTowerPosition, cameraShowTowerWayTime));

        float startTime = Time.time;
        this.RecurringActions(() =>
        {
            mainCamera.transform.LookAt(target);
            bool inProgress = Time.time - startTime <= cameraShowTowerWayTime;
            if (!inProgress)
            {
                restartCaller.SetActive(true);
            }
            return inProgress;
        });
    }

    private void PerfectMove()
    {
        interactable = false;
        StartCoroutine(PerfectMoveCoroutine());
    }

    private IEnumerator PerfectMoveCoroutine()
    {
        float iterationTime = perfectMoveAnimationTime.x + perfectMoveAnimationTime.y + timeBetweenAnimations;

        for (int i = meshes.Count - 1; i >= 0; i--)
        {
            GameMesh mesh = meshes[i];
            if (previousPerfectMesh != null && mesh != previousPerfectMesh)
            {
                IterationOfAnimation(mesh.transform);
                yield return new WaitForSeconds(iterationTime);
            }
            else
            {
                break;
            }
        }

        previousPerfectMesh = previousMesh;
        interactable = true;
    }

    // решил не переделывать на более понятный и читаемый код, потому что это лишнее время в данном случае
    private void IterationOfAnimation(Transform targetTransform, Action onEnd = null)
    {
        float targetScale = targetTransform.localScale.x + perfectMoveAnimationRange.x;

        Action secondDirectionCompleted = () =>
        {
            if (onEnd != null)
            {
                onEnd();
            }
        };

        Action firstDirectionCompleted = () =>
        {
            targetScale = targetTransform.localScale.x + perfectMoveAnimationRange.y;

            if (targetScale > maxMeshSize)
            {
                targetScale = maxMeshSize;
            }
            if (targetScale < minMeshSize)
            {
                targetScale = minMeshSize;
            }

            StartCoroutine(ScaleAnimation(targetTransform, targetScale, perfectMoveAnimationRange.y, secondDirectionCompleted));
        };

        StartCoroutine(ScaleAnimation(targetTransform, targetScale, perfectMoveAnimationRange.x, firstDirectionCompleted));
    }

    private IEnumerator ScaleAnimation(Transform transform, float targetScale, float time, Action onEnd = null)
    {
        Vector3 target = new Vector3(targetScale, transform.localScale.y, targetScale);

        float startTime = Time.time;
        float deltaTime;

        while ((deltaTime = (Time.time - startTime)) <= time)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, target, deltaTime / time);
            yield return null;
        }
        transform.localScale = target;

        if (onEnd != null)
        {
            onEnd();
        }
    }
}
